package edu.ec.istdab.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.ec.istdab.dao.IAnimalDAO;
import edu.ec.istdab.model.Animal;

@Stateful
public class AnimalDAOImpl implements IAnimalDAO,Serializable{

	@PersistenceContext(unitName = "AnimlPU")
	private EntityManager em;
	
	@Override
	public Integer registrar(Animal a) throws Exception {
		// TODO Auto-generated method stub
		em.persist(a);
		return a.getIdAnimal();
	}

	@Override
	public Integer modificar(Animal a) throws Exception {
		// TODO Auto-generated method stub
		em.merge(a);
		return 1;
	}

	@Override
	public Integer eliminar(Animal a) throws Exception {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public List<Animal> listar() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
