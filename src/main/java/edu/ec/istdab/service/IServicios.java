package edu.ec.istdab.service;

import java.util.List;

public interface IServicios<A> {
	Integer registrar(A a) throws Exception;
	Integer modificar(A a) throws Exception;
	Integer eliminar(A a) throws Exception;
	List<A> listar() throws Exception;
}
