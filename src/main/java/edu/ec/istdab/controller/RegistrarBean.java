package edu.ec.istdab.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import edu.ec.istdab.model.Animal;
import edu.ec.istdab.service.IAnimalService;

public class RegistrarBean implements Serializable{
	
	@Inject
	private IAnimalService service;
	
	private Animal animal;

	@PostConstruct
	public void init() {
		this.registrar();
	}
	
	public void registrar() {
		try {
			this.service.registrar(animal);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso: ", "Registro completado con exito"));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	
	
	public IAnimalService getService() {
		return service;
	}

	public void setService(IAnimalService service) {
		this.service = service;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	
	
	
	
	
}
