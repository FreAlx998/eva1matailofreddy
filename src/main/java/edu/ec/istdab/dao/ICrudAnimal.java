package edu.ec.istdab.dao;

import java.util.List;

public interface ICrudAnimal<A> {
	Integer registrar(A a) throws Exception;
	Integer modificar(A a) throws Exception;
	Integer eliminar(A a) throws Exception;
	List<A> listar() throws Exception;
}
