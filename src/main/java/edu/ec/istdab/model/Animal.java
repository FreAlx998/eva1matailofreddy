package edu.ec.istdab.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "animal")
public class Animal implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idAnimal;
	
	@Column(name = "tipoAnimal",nullable = false,length = 25)
	private String tipoAnimal;
	
	@Column(name = "raza",nullable = false,length = 25)
	private String raza;
	
	@Column(name = "edadAnimal",nullable = false,length = 2)
	private String edadAnimal;
	
	@Column(name = "habitat",nullable = false,length = 25)
	private String habitat;
	
	@Column(name = "due�o",nullable = false,length = 25)
	private String due�o;
	
	@Column(name = "direccionDue�o",nullable = false,length = 25)
	private String direccionDue�o;
	
	@Column(name = "estado",nullable = false,length = 1)
	private String estado;
	
	@Column(name = "sexoAnimal",nullable = false,length = 1)
	private String sexoAnimal;
	
	@Column(name = "sexoDue�o",nullable = false,length = 1)
	private String sexoDue�o;

	
	public Integer getIdAnimal() {
		return idAnimal;
	}

	public void setIdAnimal(Integer idAnimal) {
		this.idAnimal = idAnimal;
	}

	public String getTipoAnimal() {
		return tipoAnimal;
	}

	public void setTipoAnimal(String tipoAnimal) {
		this.tipoAnimal = tipoAnimal;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public String getEdadAnimal() {
		return edadAnimal;
	}

	public void setEdadAnimal(String edadAnimal) {
		this.edadAnimal = edadAnimal;
	}

	public String getHabitat() {
		return habitat;
	}

	public void setHabitat(String habitat) {
		this.habitat = habitat;
	}

	public String getDue�o() {
		return due�o;
	}

	public void setDue�o(String due�o) {
		this.due�o = due�o;
	}

	public String getDireccionDue�o() {
		return direccionDue�o;
	}

	public void setDireccionDue�o(String direccionDue�o) {
		this.direccionDue�o = direccionDue�o;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getSexoAnimal() {
		return sexoAnimal;
	}

	public void setSexoAnimal(String sexoAnimal) {
		this.sexoAnimal = sexoAnimal;
	}

	public String getSexoDue�o() {
		return sexoDue�o;
	}

	public void setSexoDue�o(String sexoDue�o) {
		this.sexoDue�o = sexoDue�o;
	}
	
}
